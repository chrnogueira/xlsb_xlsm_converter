About
------

**XLSB** files are nice, but few applications can read it. As **XLSB** files may contain macros, it is wise to convert them to **XLSM**.


How to use it
-------------

Place the **VBS** file inthe folder containing the files and be happy
Dim obj_app, obj_wkb

Set obj_fso = CreateObject("Scripting.FileSystemObject")
Set obj_app = CreateObject("Excel.Application")

obj_parent_folder = obj_fso.GetParentFolderName(wscript.ScriptFullName) 
Set obj_folder = obj_fso.GetFolder(obj_parent_folder)
Set list_files = obj_folder.Files

For Each item_file in list_files
    'verify if file is xlsb file
    if UCase(Right(item_file.Name, 4)) = "XLSB" then
        Set obj_wkb = obj_app.Workbooks.Open(item_file)
        new_name = left(item_file.Path, Len(item_file.Path) - 5) & ".xlsm"
        
        '52 is the filetype for xlsm file
        obj_wkb.SaveAs new_name, 52
        obj_wkb.Close, False

        Set obj_wkb = Nothing
    end if
Next

'quit excel application
obj_app.Quit
Set obj_app = Nothing

WScript.Echo "YARRRR"